package com.github.marcopollivier.view;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Named;

import org.primefaces.event.RowEditEvent;

import com.github.marcopollivier.controller.DepartamentoController;
import com.github.marcopollivier.controller.impl.DepartamentoControllerImpl;
import com.github.marcopollivier.model.Departamento;

@RequestScoped
@Named(value="departamentoMB")
public class DepartamentoMB implements Serializable{

	private static final long serialVersionUID = 1L;

	private Departamento entity;
	private DepartamentoController controller;
	
	@PostConstruct
	public void init() {
		entity = new Departamento();
		controller = new DepartamentoControllerImpl();
	}
	
    public List<Departamento> getDepartamentos() {
    	return controller.retrieve();
    }
	
    public void saveButton(ActionEvent actionEvent) throws IOException {
        controller.create(entity);
    	addMessage("Departamento " + entity.getName() + " adicionado com sucesso");
    	
    	FacesContext.getCurrentInstance().getExternalContext().redirect("index.jsf");
    }
    
    public void deleteButton(ActionEvent actionEvent) throws IOException {
    	controller.delete(null);
    	addMessage("Departamento " + entity.getName() + " excluído com sucesso");
    	
    	FacesContext.getCurrentInstance().getExternalContext().redirect("index.jsf");
    }
    
    public void onRowEdit(RowEditEvent event) {
    	entity = controller.update((Departamento)event.getObject());
    	addMessage("Departamento Editado");
    }
     

    
    public void onRowCancel(RowEditEvent event) {
		addMessage("Alteração cancelada");
    }
	
    public void addMessage(String summary) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary,  null);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
	
    //GETs and SETs
    public Departamento getEntity() {
		return entity;
	}
	
	public void setEntity(Departamento entity) {
		this.entity = entity;
	}
	
}

