package com.github.marcopollivier.view;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Named;

import org.primefaces.event.RowEditEvent;

import com.github.marcopollivier.controller.DepartamentoController;
import com.github.marcopollivier.controller.EmpregadoController;
import com.github.marcopollivier.controller.impl.DepartamentoControllerImpl;
import com.github.marcopollivier.controller.impl.EmpregadoControllerImpl;
import com.github.marcopollivier.model.Departamento;
import com.github.marcopollivier.model.Empregado;

@RequestScoped
@Named(value="empregadoMB")
public class EmpregadoMB implements Serializable{

	private static final long serialVersionUID = 1L;

	private Empregado entity;
	private EmpregadoController controller;
	private DepartamentoController depController;
	
	@PostConstruct
	public void init() {
		entity = new Empregado();
		controller = new EmpregadoControllerImpl();
		depController = new DepartamentoControllerImpl();
	}
	
    public List<Empregado> getEmpregados() {
    	return controller.retrieve();
    }
    
    public List<Departamento> getDepartamentos() {
    	return depController.retrieve();
    }
	
    public void saveButton(ActionEvent actionEvent) throws IOException {
        controller.create(entity);
    	addMessage("Empregado " + entity.getName() + " adicionado com sucesso");
    	FacesContext.getCurrentInstance().getExternalContext().redirect("index.jsf");
    }
    
    public void onRowEdit(RowEditEvent event) {
    	entity = controller.update((Empregado)event.getObject());
    	addMessage("Empregado Editado");
    }
     
    public void onRowCancel(RowEditEvent event) {
		addMessage("Edição cancelada");
    }
	
    public void addMessage(String summary) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary,  null);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
	
    //GETs and SETs
    public Empregado getEntity() {
		return entity;
	}
	
	public void setEntity(Empregado entity) {
		this.entity = entity;
	}
	
}