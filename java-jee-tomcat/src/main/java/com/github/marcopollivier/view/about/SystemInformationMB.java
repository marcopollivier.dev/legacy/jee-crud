package com.github.marcopollivier.view.about;

import java.io.Serializable;
import java.util.Date;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

@RequestScoped
@Named(value="about")
public class SystemInformationMB implements Serializable {
	private static final long serialVersionUID = 1L;

	public String getName() {
		return "Marco Paulo Ollivier - " + new Date();
	}


}