package com.github.marcopollivier.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Departamento implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private Integer id;
	
	private String name;
	
	@OneToMany(mappedBy="departamento", cascade=CascadeType.PERSIST)
	private List<Empregado> empregados;
	
	public Departamento() {
		super();
		empregados = new ArrayList<>();
	}
	
	public Departamento(String name) {
		this();
		this.setName(name);
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public List<Empregado> getEmpregados() {
		return empregados;
	}
	
	public void setEmpregados(List<Empregado> empregados) {
		this.empregados = empregados;
	}
	
}
