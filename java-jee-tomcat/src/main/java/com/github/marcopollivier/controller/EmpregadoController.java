package com.github.marcopollivier.controller;

import java.util.List;

import com.github.marcopollivier.model.Empregado;

public interface EmpregadoController {

	public Empregado create(Empregado emp);
	public Empregado update(Empregado emp);
	public Empregado retrieve(Integer id);
	public List<Empregado> retrieve();
	public void delete(Empregado emp);
	
}
