package com.github.marcopollivier.controller;

import java.util.List;

import com.github.marcopollivier.model.Departamento;

public interface DepartamentoController {

	public Departamento create(Departamento dpto);
	public Departamento update(Departamento dpto);
	public Departamento retrieve(Integer id);
	public List<Departamento> retrieve();
	public void delete(Departamento dpto);
	
}
