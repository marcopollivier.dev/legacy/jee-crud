package com.github.marcopollivier.controller.impl;

import java.util.List;

import com.github.marcopollivier.controller.EmpregadoController;
import com.github.marcopollivier.dao.EmpregadoDAO;
import com.github.marcopollivier.dao.impl.EmpregadoDAOImpl;
import com.github.marcopollivier.model.Empregado;

public class EmpregadoControllerImpl implements EmpregadoController {

	private EmpregadoDAO dao;
	
	public EmpregadoControllerImpl() {
		dao = new EmpregadoDAOImpl();
	}
	
	@Override
	public Empregado create(Empregado empregado) {
		return dao.create(empregado);
	}

	@Override
	public Empregado update(Empregado empregado) {
		return dao.update(empregado);
	}

	@Override
	public Empregado retrieve(Integer id) {
		return dao.retrieve(id);
	}

	@Override
	public List<Empregado> retrieve() {
		return dao.retrieve();
	}

	@Override
	public void delete(Empregado empregado) { 
		dao.delete(empregado);
	}


}
