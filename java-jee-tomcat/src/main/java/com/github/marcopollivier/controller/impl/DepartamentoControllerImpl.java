package com.github.marcopollivier.controller.impl;

import java.util.List;

import com.github.marcopollivier.controller.DepartamentoController;
import com.github.marcopollivier.dao.DepartamentoDAO;
import com.github.marcopollivier.dao.impl.DepartamentoDAOImpl;
import com.github.marcopollivier.model.Departamento;

public class DepartamentoControllerImpl implements DepartamentoController {

	private DepartamentoDAO dao;
	
	public DepartamentoControllerImpl() {
		dao = new DepartamentoDAOImpl();
	}
	
	@Override
	public Departamento create(Departamento dpto) {
		return dao.create(dpto);
	}

	@Override
	public Departamento update(Departamento dpto) {
		return dao.update(dpto);
	}

	@Override
	public Departamento retrieve(Integer id) {
		return dao.retrieve(id);
	}

	@Override
	public List<Departamento> retrieve() {
		return dao.retrieve();
	}

	@Override
	public void delete(Departamento dpto) { 
		dao.delete(dpto);
	}
	
}
