package com.github.marcopollivier.dao;

import java.util.List;

import com.github.marcopollivier.model.Departamento;

public interface DepartamentoDAO {

	public Departamento retrieve(Integer id);
	public List<Departamento> retrieve();
	public Departamento create(Departamento dpto);
	public Departamento update(Departamento dpto);
	public void delete(Departamento dpto);
	
}
