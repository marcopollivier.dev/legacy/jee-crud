package com.github.marcopollivier.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaQuery;

import com.github.marcopollivier.dao.EmpregadoDAO;
import com.github.marcopollivier.dao.EntityManagerProducer;
import com.github.marcopollivier.model.Empregado;

public class EmpregadoDAOImpl implements EmpregadoDAO {

	EntityManager entityManager;
	
	@Override
	public Empregado retrieve(Integer id) {
		entityManager = EntityManagerProducer.entityManagerFactory().createEntityManager();
		
		Empregado finded = entityManager.find(Empregado.class, id);
		entityManager.close();
		return finded;
	}

	@Override
	public Empregado create(Empregado empregado) {
		entityManager = EntityManagerProducer.entityManagerFactory().createEntityManager();
		
		entityManager.getTransaction().begin();    
		entityManager.persist(empregado);
		entityManager.getTransaction().commit();  
	
		entityManager.close();
		
		return empregado;
	}

	@Override
	public Empregado update(Empregado empregado) {
		entityManager = EntityManagerProducer.entityManagerFactory().createEntityManager();
		
		entityManager.getTransaction().begin();    
		entityManager.merge(empregado);
		entityManager.getTransaction().commit();  
	
		entityManager.close();
		
		return empregado;
	}
	
	@Override
	public void delete(Empregado empregado) {
		entityManager = EntityManagerProducer.entityManagerFactory().createEntityManager();
		
		entityManager.getTransaction().begin();    
		entityManager.remove(entityManager.find(Empregado.class, empregado.getId()));
		entityManager.getTransaction().commit();  
	
		entityManager.close();
	}

	@Override
	public List<Empregado> retrieve() {
		entityManager = EntityManagerProducer.entityManagerFactory().createEntityManager();
		
		CriteriaQuery<Empregado> criteria = entityManager.getCriteriaBuilder().createQuery(Empregado.class);
	    criteria.select(criteria.from(Empregado.class));
	    List<Empregado> result = entityManager.createQuery(criteria).getResultList();
		
		entityManager.close();
		return result;
	}
	
}