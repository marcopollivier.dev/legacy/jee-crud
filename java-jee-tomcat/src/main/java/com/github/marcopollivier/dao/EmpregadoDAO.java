package com.github.marcopollivier.dao;

import java.util.List;

import com.github.marcopollivier.model.Empregado;

public interface EmpregadoDAO {

	public Empregado retrieve(Integer id);
	public List<Empregado> retrieve();
	public Empregado create(Empregado empregado);
	public Empregado update(Empregado empregado);
	public void delete(Empregado empregado);
	
}
