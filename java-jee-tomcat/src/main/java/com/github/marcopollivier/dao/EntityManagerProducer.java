package com.github.marcopollivier.dao;

import java.io.Serializable;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class EntityManagerProducer implements Serializable {

	private static final long serialVersionUID = 1L;

	//sem injecao
	public static EntityManagerFactory entityManagerFactory() {
		return Persistence.createEntityManagerFactory("jee-crud");
	}
	
}