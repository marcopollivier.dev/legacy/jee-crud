package com.github.marcopollivier.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaQuery;

import com.github.marcopollivier.dao.DepartamentoDAO;
import com.github.marcopollivier.dao.EntityManagerProducer;
import com.github.marcopollivier.model.Departamento;

public class DepartamentoDAOImpl implements DepartamentoDAO {

	EntityManager entityManager;
	
	public DepartamentoDAOImpl() {
	}
	
	@Override
	public Departamento retrieve(Integer id) {
		entityManager = EntityManagerProducer.entityManagerFactory().createEntityManager();
		Departamento finded = entityManager.find(Departamento.class, id);
		entityManager.close();
		return finded;
	}
	
	@Override
	public List<Departamento> retrieve() {
		entityManager = EntityManagerProducer.entityManagerFactory().createEntityManager();
		
		CriteriaQuery<Departamento> criteria = entityManager.getCriteriaBuilder().createQuery(Departamento.class);
	    criteria.select(criteria.from(Departamento.class));
	    List<Departamento> ListOfEmailDomains = entityManager.createQuery(criteria).getResultList();
		
		entityManager.close();
		return ListOfEmailDomains;
	}
	
	public Integer countDepWithoutEmpregado() {
//		SELECT count(*)
//		FROM Departamento as dpto
//		LEFT JOIN Empregado as emp
//		ON dpto.id=emp.fk_dpto
//		WHERE emp.id is null;

		return null;
	}

	@Override
	public Departamento create(Departamento dpto) {
		entityManager = EntityManagerProducer.entityManagerFactory().createEntityManager();
		
		entityManager.getTransaction().begin();    
		entityManager.persist(dpto);
		entityManager.getTransaction().commit();  
	
		entityManager.close();
		
		return dpto;
	}


	@Override
	public Departamento update(Departamento dpto) {
		entityManager = EntityManagerProducer.entityManagerFactory().createEntityManager();
		
		entityManager.getTransaction().begin();
		entityManager.merge(dpto);
		entityManager.getTransaction().commit();  
	
		entityManager.close();
		
		return dpto;
	}

	@Override
	public void delete(Departamento dpto) {
		entityManager = EntityManagerProducer.entityManagerFactory().createEntityManager();
		
		entityManager.getTransaction().begin();
		entityManager.remove(entityManager.find(Departamento.class, dpto.getId()));
		entityManager.getTransaction().commit();  
	
		entityManager.close();
	}
	
}
