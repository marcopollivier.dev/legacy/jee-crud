package com.github.marcopollivier.dao;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.Test;

public class EntityManagerConnectionTest {

	@Test
	public void connectionTest() {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("jee-crud");
		factory.close();
	}

}