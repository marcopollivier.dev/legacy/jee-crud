package com.github.marcopollivier.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import javax.validation.ConstraintViolationException;

import org.junit.Before;
import org.junit.Test;

import com.github.marcopollivier.dao.impl.DepartamentoDAOImpl;
import com.github.marcopollivier.dao.impl.EmpregadoDAOImpl;
import com.github.marcopollivier.model.Departamento;
import com.github.marcopollivier.model.Empregado;

public class EmpregadoDAOTest {

	//TODO Injetar dependencia
	private EmpregadoDAO dao;
	private DepartamentoDAO depDao;
	
	@Before
	public void init() {
		dao = new EmpregadoDAOImpl();
		depDao = new DepartamentoDAOImpl();
	}

	@Test
	public void persistTest() {
		Departamento dpto = new Departamento("dep 123");
		dpto = depDao.create(dpto);
		assertNotNull(dpto.getId());
		
		Empregado empregado = new Empregado("Empregado 123");
		empregado.setDepartamento(dpto);
		empregado = dao.create(empregado);
		assertNotNull(empregado.getId());
	}
	
	@Test (expected = ConstraintViolationException.class)
	public void persistDeptoNullTest() {
		Empregado empregado = new Empregado("Empregado 123");
		empregado = dao.create(empregado);
		assertNotNull(empregado.getId());
	}
	
	@Test
	public void retrieveTest() {
		Empregado empregado = dao.retrieve(1);
		assertNotNull(empregado);
	}
	
	@Test
	public void updateTest() {
		String novoNome = "Nome alterado";
		
		Empregado empregado = dao.retrieve(1);
		empregado.setName(novoNome);
		
		Empregado new_empregado = dao.update(empregado);
		
		assertEquals(new_empregado.getName(), novoNome);
	}
	
	@Test
	public void deleteTest() {
		Departamento dpto = new Departamento("dep 123");
		dpto = depDao.create(dpto);
		assertNotNull(dpto.getId());
		
		Empregado empregado = new Empregado("Empregado");
		empregado.setDepartamento(dpto);
		empregado = dao.create(empregado);
		assertNotNull(empregado.getId());
		
		dao.delete(dao.retrieve(empregado.getId()));
		
		Empregado deletado = dao.retrieve(empregado.getId());
		assertNull(deletado);
	}
	
}