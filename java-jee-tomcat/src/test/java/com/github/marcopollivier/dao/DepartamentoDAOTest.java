package com.github.marcopollivier.dao;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import javax.persistence.RollbackException;

import org.junit.Before;
import org.junit.Test;

import com.github.marcopollivier.dao.impl.DepartamentoDAOImpl;
import com.github.marcopollivier.dao.impl.EmpregadoDAOImpl;
import com.github.marcopollivier.model.Departamento;
import com.github.marcopollivier.model.Empregado;

public class DepartamentoDAOTest {

	//TODO Injetar dependencia
	DepartamentoDAO dao;
	
	@Before
	public void init() {
		dao = new DepartamentoDAOImpl();
	}

	@Test
	public void persistTest() {
		Departamento dpto = new Departamento("dep 123");
		dpto = dao.create(dpto);
		assertNotNull(dpto.getId());
	}
	
	@Test
	public void retrieveTest() {
		Departamento dpto = dao.retrieve(1);
		assertNotNull(dpto);
	}
	
	@Test
	public void retrieveNonexistentDepTest() {
		Departamento dpto = dao.retrieve(9999);
		assertNull(dpto);
	}
	
	@Test
	public void deleteTest() {
		Departamento dpto = new Departamento("dep 123");
		dpto = dao.create(dpto);
		assertNotNull(dpto.getId());
		
		dao.delete(dao.retrieve(dpto.getId()));
		
		Departamento deletado = dao.retrieve(dpto.getId());
		assertNull(deletado);
	}

	@Test (expected = RollbackException.class)
	public void deleteWithEmployeeTest() {
		Departamento dpto = new Departamento("Departamento com funcionarios");
		dpto = dao.create(dpto);
		assertNotNull(dpto.getId());

		Empregado empregado = new Empregado("Empregado 123");
		empregado.setDepartamento(dpto);
		empregado = new EmpregadoDAOImpl().create(empregado);
		assertNotNull(empregado.getId());
		
		dao.delete(dpto);
	}
	
}
